package blackjack;

import java.util.*;

public class dealer extends user{
	
	static Random rand = new Random();

	//used to place a random 0 or 1 to decide if the dealer hits
	static int dealerMove;
	
	//carries the dealer's hand
	static public ArrayList<card> dealerHand = new ArrayList<card>();
	
	//while true, dealer's turn will continue
	static boolean dealerActive = true;
	
	//current deck being used
	static card[] cd = deck.currentDeck;
	
	//sum of cards in dealer's hand
	static int dealerSum;
	
	public static void turn()
	{
		while(dealerActive == true)
		{
			//picks a random number to decide the dealer's move
			dealerMove = rand.nextInt(2);
	        dealerSum = sumHand(dealerHand, dealerSum);
	        //if the dealer's hand totals less than 17 and their random move is hit, dealer hits
	        if(dealerMove == 0 || dealerSum < 17)
	        {
	        	System.out.println("Dealer hits");
	        	dealerHand = deal(dealerHand);
	        	dealerSum = sumHand(dealerHand, dealerSum);
	        	//if dealer busts, handles accordingly
	        	if(dealerSum > 21)
	        	{
	        		dealerActive = false;
	        		System.out.println("dealer bust");
	        		System.out.println("You win! dealer had " + dealerHand);
	        		System.out.println("Dealer sum: " + dealerSum);
	        		user.userCash += bet;
	        	}
	        }
	        else
	        {
	        	//if the dealer's random move is stand and hand value is greater than 17, ends turn and pays out accordingly
	        	dealerActive = false;
	        	if(user.sum > dealerSum)
	        	{
	        		System.out.println("You win! dealer had " + dealerHand);
	        		System.out.println("Dealer sum: " + dealerSum);
	        		userCash += bet;
	        	}
	        	
	        	else if(user.sum == dealerSum)
	        	{
	        		System.out.println("You and the dealer have equal sums, push");
	        	}
	        	else
	        	{
	        		System.out.println("You lose, dealer had " + dealerHand);
	        		System.out.println("Dealer sum: " + dealerSum);
	        		userCash -= bet;
	        	}
	        }
		}
	}
	
	public static void initialBlackjack()
	{
		//checks if any blackjack's are dealt out initially
		user.sum = sumHand(user.hand, user.sum);
		dealerSum = sumHand(dealerHand, dealerSum);
		int userInitial = user.sum;
		int dealerInitial = dealerSum;
		if(userInitial == 21)
		{
			if(dealerInitial == 21)
			{
				//if both of you have blackjack, ends turn without payout
				System.out.println("Both you and the dealer have blackjack");
				user.turnActive = false;
				dealerActive = false;
			}
			else
			{
				//if only you got blackjack, pays out accordingly
				System.out.println("You got blackjack, you win!");
				user.userCash += bet;
				user.turnActive = false;
				dealerActive = false;
		    }
		}
		else if (dealerInitial == 21)
		{
			//if just the dealer got blackjack, pays out accordingly
			System.out.println("Dealer has blackjack" + dealerHand);
		    user.userCash -= bet;
		    user.turnActive = false;
			dealerActive = false;
		}
	}
	
	public static void startTurn()
	{
		System.out.println(" ");

		//Initializes deck and hands for a new turn
		deck.deckReset();
		user.hand.clear();
		dealer.dealerHand.clear();
		
		//deals in the order according the rules of blackjack
		user.hand = user.deal(user.hand);
		dealer.dealerHand = dealer.deal(dealer.dealerHand);
		System.out.println("Dealer's first card is: " + dealer.dealerHand);
		user.hand = user.deal(user.hand);
		dealer.dealerHand = dealer.deal(dealer.dealerHand);
		
		//prints out your relevant information
		System.out.println("Your hand is: " + user.hand);
		user.sum = user.sumHand(user.hand, user.sum);
		System.out.println("Sum: " + user.sum);
	    System.out.println("You have $" + user.userCash);
	    
	    //resets bet to 10 and allows turns to continue
	    user.bet = 10;
	    user.turnActive = true;
	    dealer.dealerActive = true;
	    
	    //checks if blackjack has been dealt
	    dealer.initialBlackjack();
	}
}
