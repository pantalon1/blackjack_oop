package blackjack;

public class deck {
	//array representing possible suits to be assigned to cards
	static private String[] suits = {"heart", "diamond", "club", "spade"};

	//creates an array to represent a deck to be used in a hand
    static public card[] currentDeck = new card[52];
    
    static void deckReset()
    {
    	//fills deck with appropriate values, types, and suits
    	for (int i =0; i<52; i++)
    	{
    		currentDeck[i] = new card(0, " ", " ");
    		if(i<4)
    		{
    			currentDeck[i].value = 2;
    			currentDeck[i].type = "number";
    		}
    		else if(i>=4 && i<8)
    		{
    			currentDeck[i].value = 3;
    			currentDeck[i].type = "number";
    		}
    		else if(i>=8 && i<12)
    		{
    			currentDeck[i].value = 4;
    			currentDeck[i].type = "number";
    		}
    		else if(i>=12 && i<16)
    		{
    			currentDeck[i].value = 5;
    			currentDeck[i].type = "number";
    		}
    		else if(i>=16 && i<20)
    		{
    			currentDeck[i].value = 6;
    			currentDeck[i].type = "number";
    		}
    		else if(i>=20 && i<24)
    		{
    			currentDeck[i].value = 7;
    			currentDeck[i].type = "number";
    		}
    		else if(i>=24 && i<28)
    		{
    			currentDeck[i].value = 8;
    			currentDeck[i].type = "number";
    		}
    		else if(i>=28 && i<32)
    		{
    			currentDeck[i].value = 9;
    			currentDeck[i].type = "number";
    		}
    		else if(i>=32 && i<36)
    		{
    			currentDeck[i].value = 10;
    			currentDeck[i].type = "number";
    		}
    		else if(i>=36 && i<40)
    		{
    			currentDeck[i].value = 10;
    			currentDeck[i].type = "jack";
    		}
    		else if(i>=40 && i<44)
    		{
    			currentDeck[i].value = 10;
    			currentDeck[i].type = "queen";
    		}
    		else if(i>=44 && i<48)
    		{
    			currentDeck[i].value = 10;
    			currentDeck[i].type = "king";
    		}
    		else if(i>=48 && i<52)
    		{
    			currentDeck[i].value = 11;
    			currentDeck[i].type = "ace";
    		}
    		
    		//rotates through suit array for each step in loop, so each unique card value+type has one of each suit
    		currentDeck[i].suit = suits[i%4];
    	}
    }

}