package blackjack;

import java.util.*;

public class user {
	
	static Random rand = new Random();
	
	//Initializes the user's hand
	static public ArrayList<card> hand = new ArrayList<card>();
	
	//Initializes the current deck for the hand
	static card[] cd = deck.currentDeck;
	
	//user's bank
    static int userCash = 100;

    //tracks the value of the bet on a given hand
    static int bet = 10;

    //while true, turn will continue. When false, turn will end
    static boolean turnActive = true;
    
    //sum of cards in hand
    static int sum = 0;
    
    //tracks the second card in a hand that the user splits, so it can be used as the first card in the subsequent hand on the same turn
    public static card splitCard = new card(0, " ", " ");
	
	//Deal function is used to deal cards into both user and dealer's hands
	public static ArrayList<card> deal(ArrayList<card> currentHand)
	{
		int rand1 = rand.nextInt(52);
		//picks a random number representing a card in the deck, if that card hasn't been drawn already (value zero) continues
		while (cd[rand1].value < 1)
		{
			rand1 = rand.nextInt(52);
		}
		//deep copy of drawn card, as using pointer deletes card before appending to hand
		card cardDraw = new card (cd[rand1].value, cd[rand1].suit, cd[rand1].type);

		//appends hand with the card dealt
		currentHand.add(cardDraw);
		
		//removes card value from deck so it won't be dealt again
		cd[rand1].value = 0;

		return currentHand;
	}
	
	//Sum hand function loops through cards in a hand and returns their sum, also accounting for aces and subtracts 10 from the total if an ace causes a bust. Ace counter is used to track how many aces are in the hand.
	protected static int sumHand(ArrayList<card> currentHand, int currentSum)
	{
		currentSum = 0;
		int aceCounter = 0;
		for(int i = 0; i<(currentHand.size()); i++)
		{
			//adds the value of each card to the sum
			currentSum += currentHand.get(i).value;

			//tracks number of aces in the hand
			if(currentHand.get(i).type == "ace")
			{
				aceCounter += 1;
			}

			//if user busts without any aces, ends the sumHand call
			if(currentSum > 21 && aceCounter == 0)
			{
				break;
			}

			//replaces ace value of 11 with 1 when appropriate, according to the rules of blackjack
			else if(currentSum > 21 && aceCounter > 0)
			{
				while(currentSum > 21 && aceCounter > 0)
				{
					currentSum -= 10;
					aceCounter -= 1;
				}
			}
		}

		return currentSum;

	}
	
	//endHand ends a turn after a winner has been decided, and allows the user to exit, and returns their final winnings
	private static void endHand()
	{
		
		System.out.println("Enter 1 to play another hand, or 2 to quit");
		Scanner in = new Scanner(System.in);

		String input2 = in.nextLine();

		if(input2.equals("1")==false)
		{
			System.out.print("You finished with $");
			System.out.println(userCash);
			userCash = 0;
		}


	}
	
	//checks if the user had split their initial hand when their turn ends
	//then initiates second turn with new hand with split card
	private static void splitCheck(card[] cd)
	{
        //restarts the user's turn with the split hand
        if (splitCard.value != 0 && user.turnActive == false)
        {
        	dealer.dealerActive = true;
        	turnActive = true;
        	hand.clear();
        	//deep copy so splitcard doesn't get cleared before appending to hand
        	card addSplitCard = new card(splitCard.value, splitCard.suit, splitCard.type);
        	//appends hand with split card for second play through
        	hand.add(addSplitCard);
        	hand = deal(hand);
        	System.out.println("Your hand is now " + hand);
        	//clears splitcard
        	splitCard.value = 0;
        	splitCard.suit = " ";
        	splitCard.type = " ";
        }
        else if (turnActive == false)
        {
        	endHand();
        }
	}
	
	public static void split()
	{
		//First, checks if you haven't hit yet, and that your starting hand has equal ranks
		if(hand.get(0).value == hand.get(1).value && hand.size()<3)
        {
			//deep copy of split card
			splitCard = new card (hand.get(1).value, hand.get(1).suit, hand.get(1).type);
			//removes second card from hand, to be appended after the end of this turn
			hand.remove(1);
			//deals a new secondary card to your hand
			hand = deal(hand);
			System.out.println("Your hand is now " + hand);
        }
        else
        {
        	System.out.println("Can't split without having equal starting cards");
        }
	}
	
	public static void hit()
	{
		//deals a card
		hand = deal(hand);
        System.out.println(hand);
        sum = sumHand(hand, sum);
        System.out.println("sum " + sum);

        //if bust, pays out accordingly
        if(sum > 21)
        {
          turnActive = false;
          System.out.println("bust");
          userCash -= bet;
        }
        //if you hit blackjack, checks if dealer has blackjack, and pays out/pushes accordingly
        else if(sum == 21)
        {
        	turnActive = false;
        	System.out.println("Blackjack");
        	dealer.turn();
        	if(dealer.dealerSum == 21)
        	{
        		bet = 0;
        		System.out.println("Dealer hit blackjack as well, push.");
        		System.out.println("Dealer had " + dealer.dealerHand);
        	}
        	userCash += bet;
        }
        splitCheck(cd);
	}
	
	public static void stand()
	{
		//finds sum, and calls dealer turn to continue
		System.out.println("user hand: "+user.hand);
		System.out.println("dealer hand: "+dealer.dealerHand);
		sum = sumHand(hand, sum);
		turnActive = false;
		dealer.turn();
		splitCheck(cd);
	}
	
	public static void doubleDown()
	{
		//doubles bet if possible
		if(bet*2<userCash)
        {
			bet = bet*2;
			System.out.println("Bet is now $" + bet);
        }
        else
        {
        	System.out.println("Can't double down");
        	System.out.println("You have $" + userCash);
        	System.out.println("Bet is $" + bet);
        }
	}
	
	public static void surrender()
	{
		//halves bet and ends turn if this is your first move
		if(hand.size()<3)
        {
			userCash -= bet*0.5;
			System.out.println("You lose $" + bet*0.5);
			turnActive = false;
			endHand();
        }
        else
        {
        	System.out.println("Can't surrender after hitting");
        }
	}
}
