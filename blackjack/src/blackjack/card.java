package blackjack;

public class card{
	public int value;
	public String suit;
	public String type;
	
	//constructor used to make cards
	card(int value, String suit, String type){
		this.value = value;
		this.suit = suit;
		this.type = type;
	}
	
	//following three functions get attributes of card to be used in toString override
	public int getValue()
	{
		return value;
	}
	
	public String getSuit()
	{
		return suit;
	}
	
	public String getType()
	{
		return type;
	}
	
	//overrides how cards are converted to strings, so information is printed correctly
	@Override
	public String toString() {
	    return "value: " + this.getValue() + 
	           ", suit: " + this.getSuit() +
	           ", type: " + this.getType(); 
	}
}
