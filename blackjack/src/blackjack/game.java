package blackjack;

import java.util.*;

public class game {
	
	Scanner in = new Scanner(System.in);

	public static void main(String[] args) {
		while(user.userCash > 0)
	    {
			dealer.startTurn();
		    
		    while (user.turnActive == true)
		    {
		    	System.out.println("1 = Hit, 2 = Stand, 3 = Double Down, 4 = Split, 5 = Surrender");
		        Scanner in = new Scanner(System.in);

		        String input = in.nextLine();
		        
		        if(input.equals("1"))
		        {
		        	user.hit();
		        }
		        else if(input.equals("2"))
		        {
		        	user.stand();
		        }
		        else if(input.equals("3"))
		        {
		        	user.doubleDown();
		        }
		        else if(input.equals("4"))
		        {
		        	user.split();
		        }
		        else if(input.equals("5"))
		        {
		        	user.surrender();
		        }
		        else
		        {
		        	System.out.println("invalid input");
		        }
		    }
	    }

	}


}
